## GIT
cd ..
git clone https://bitbucket.org/ev45ive/sages-vue-czerwiec.git sages-vue-czerwiec
cd sages-vue-czerwiec
npm i 
npm run serve

## Git update
git stash -u && git pull 

 https://us02web.zoom.us/j/83414809692 

https://bit.ly/ 2U MK hd D

node -v 
v14.15.4

npm -v
6.14.10

code -v
1.40.1

git --version
git version 2.31.1.windows.1

chrome 
W
## VS Code extensions
https://marketplace.visualstudio.com/items?itemName=octref.vetur
https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets

# CLI - automatyzacje
https://cli.vuejs.org/
npm install -g @vue/cli

vue --version
@vue/cli 4.5.13

vue --help
vue ui
<!-- lub -->
vue create nazwa-projektu

## Bootstrap CSS
npm i bootstrap

## Emmet / ZenCoding
.container>.row>.col
https://docs.emmet.io/cheat-sheet/

## Chrome devtools
https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

## Lifecycle
https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram  

## Quicktype
https://quicktype.io/typescript

## Plugins
https://github.com/vuejs/awesome-vue#components--libraries
https://github.com/se-panfilov/vue-notifications
https://github.com/Akryum/v-tooltip 
https://github.com/dzwillia/vue-simple-spinner
https://vuetifyjs.com/en/components/calendars/#day-body
## Http
fetch, jQuery, Axios, vue-resource

https://github.com/pagekit/vue-resource

npm install vue-resource

## OAuth2.0
https://developer.spotify.com/documentation/general/guides/authorization-guide/
https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app

## TypeScript
https://vuejs.org/v2/guide/typescript.html


## Vue Router
https://router.vuejs.org/installation.html#direct-download-cdn


## Vuex
https://vuex.vuejs.org/