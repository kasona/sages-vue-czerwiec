```js
obj = {
    _secret:'ala ma kota',
    
    get value(){
        return this._secret.split('').reverse().join('')
    },

    set value(newValue){
        return this._secret = newValue.split('').reverse().join('')
    }
}
``

obj.value = 'Ala ma kota'
console.log(obj.value)
console.log(obj)  
//  Ala ma kota
//  {_secret: "atok am alA"}

