
export interface Playlist {
  id: string;
  name: string;
  public: boolean;
  description: string;
}

export default Playlist