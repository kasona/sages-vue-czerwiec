import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counter: 0
  },
  mutations: {
    increment(state) {
      state.counter += 1;
    },
    // on websocket push update user everywhere:
    updateUser(state, user) {
      // state.user = user;
      // $store.commit('updateUser', userData)
    },
  },
  actions: {
    loadCounter({ commit }) {
      let i = 0
      const handle = setInterval(() => {
        commit('increment')
        if (i++ > 5) {
          clearInterval(handle)
        }
      }, 1000)
    }
  },
  modules: {
  }
})
