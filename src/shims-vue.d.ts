import Vue from 'vue'

declare module "vue/types/vue" {
  interface Vue {
    showError(config?: {
      title?: string;
      message?: string;
      type?: string;
    }): void,
    user: {},
    getUser(): {
      name: string;
    }
  }

  interface VueConstructor {
    getUser(): {
      name: string;
    },
    user: {},
  }
}
declare module "vue/types/options" {
  interface ComponentOptions<V extends Vue> {
    notifications?: ({
      [key: string]: {
        title: string;
        message: string;
        type: string;
      }
    })
  }
}
