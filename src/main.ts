import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

Vue.filter('yesno', function (value: boolean) {
  console.log('fitler');
  return value ? "Yes" : "No";
})

import Button from './components/Button.vue'
Vue.component('Button', Button)

import VueResource from 'vue-resource'
Vue.use(VueResource, {});

initAuth();

((Vue as any).http).interceptors.push((request: any) => {
  debugger
  request.headers.set('Authorization', "Bearer " + getToken())
})

import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import { Http, HttpInterceptor } from 'vue-resource/types/vue_resource'
import { getToken, initAuth } from './services/AuthService'

const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}
function toast({ title, message, type, timeout, cb }: any) {
  return miniToastr[type](message, title, timeout, cb)
}
miniToastr.init({ types: toastTypes })
Vue.use(VueNotifications, {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
})

const app = new Vue({
  data() {
    return {
      user: {}
    }
  },
  methods: {
    getUser(this: Vue) {
      this.$http.get('https://api.spotify.com/v1/me').then(resp => {
        this.user = resp.data
      })
    }
  },
  router,
  store,
  render: h => h(App)
})

app.$mount('#app')