import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Playlists from '../views/Playlists.vue'
import MusicSearch from '../views/MusicSearch.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    // component: Home
    redirect: '/search',
  }, {
    path: '/playlists',
    name: 'Playlists',
    component: Playlists,
    meta: { mojeopcje: 123 }
  }, {
    path: '/search',
    name: 'Search',
    component: MusicSearch
  }, {
    path: '/albums/:album_id',
    name: 'Album',
    component: MusicSearch,
    // beforeEnter: (to, from, next) => {
    //   // ...
    // }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/access_token',
    redirect: '/search'
  },
  {
    path: '*',
    // component: PageNotFound
    redirect: '/search'
  }
]

const router = new VueRouter({
  routes,
  // mode:'hash'
  mode: 'history',
  linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  // https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards
  next()
})

export default router
