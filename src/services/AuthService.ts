

// let token: string | null = ''
let token = ''

export function getToken() {
  return token
}

export function logIn() {
  // https://developer.spotify.com/documentation/general/guides/authorization-guide/
  const params = new URLSearchParams({
    client_id: '2dd099364eab465d94112ccc0bddee64',
    response_type: 'token',
    redirect_uri: window.location.origin + '/',
    scope: '',
    show_dialog: 'true'
  })
  const url = `https://accounts.spotify.com/authorize?${params.toString()}`
  window.location.href = (url);
}

export function logOut() {

}

export function initAuth() {
  if (window.location.hash) {
    token = new URLSearchParams(window.location.hash.replace(/^#\/?/, '')).get('access_token') || ''
    window.location.hash = ''
    if (token) {
      sessionStorage.setItem('token', JSON.stringify(token))
    }
  }
  if (!token) {
    token = JSON.parse(sessionStorage.getItem('token') || '""')
  }
  if (!token) { return logIn() }
}

